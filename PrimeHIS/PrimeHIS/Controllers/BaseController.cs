﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using PrimeHIS.Dto;
using PrimeHIS.Dto.Auth;

namespace PrimeHIS.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class BaseController : ControllerBase
    {
        protected AuthenticateResponse CurrentUser => (AuthenticateResponse)HttpContext.Items["User"];
    }
}
