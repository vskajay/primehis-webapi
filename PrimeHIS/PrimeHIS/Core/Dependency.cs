﻿using Microsoft.Extensions.DependencyInjection;
using PrimeHIS.Services;
using PrimeHIS.Services.Infrastructure;

namespace PrimeHIS.Core
{
    public class Dependency
    {
        public Dependency(IServiceCollection serviceCollection)
        {
            ResolveDependency(serviceCollection);
        }
        public void ResolveDependency(IServiceCollection services)
        {
            services.AddTransient<IAuthService,AuthService>();
        }
    }
}